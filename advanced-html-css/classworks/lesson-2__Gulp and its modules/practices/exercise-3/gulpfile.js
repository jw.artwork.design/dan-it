const gulp = require("gulp");
const cssnano = require("cssnano");
const rename = require("gulp-rename");
const gulpPostcss = require('gulp-postcss');
const del = require('del');

const moveJS = () => {
	return gulp.src("./src/scripts/*.js")
		.pipe(gulp.dest("./dist/js/"));
}

const styles = () => {
	return gulp.src("./src/css/*.css")
		.pipe(gulpPostcss([cssnano]))
		.pipe(rename({suffix: ".min"}))
		.pipe(gulp.dest("./dist/css/"));
}

const moveIMG = () => {
	return gulp.src("./src/images/**/*.+(jpg|png)")
		.pipe(gulp.dest("./dist/images/"));
}

const clear = () => {
	return del('./dist', { force: true });
}

const watchers = () => {
	gulp.watch('./src/css/*.css', styles);
	gulp.watch('./src/image/**/*.jpg', moveIMG);
	gulp.watch('./src/scripts/*.js', moveJS);
}

const build = gulp.series(moveJS, moveIMG, styles);
const dev = gulp.series(clear, build, watchers);

exports.moveJS = moveJS;
exports.moveIMG = moveIMG;
exports.styles = styles;
exports.clear = clear;
exports.build = build;
exports.dev = dev;

