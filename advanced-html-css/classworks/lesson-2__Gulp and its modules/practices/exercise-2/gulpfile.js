const gulp = require("gulp");

const moveJS = () => {
	return gulp.src("./src/scripts/*.js")
		.pipe(gulp.dest("./dist/js/"));
}

const moveCSS = () => {
	return gulp.src("./src/css/*.css")
		.pipe(gulp.dest("./dist/css/"));
}

const moveIMG = () => {
	return gulp.src("./src/images/**/*.+(jpg|png)")
		.pipe(gulp.dest("./dist/images/"));
}

const watchers = () => {
	gulp.watch('./src/css/*.css', moveCSS);
	gulp.watch('./src/image/**/*.jpg', moveIMG);
	gulp.watch('./src/scripts/*.js', moveJS);
}

const build = gulp.series(moveJS, moveIMG, moveCSS);
const dev = gulp.series(moveJS, moveIMG, moveCSS, watchers);

exports.moveJS = moveJS;
exports.moveIMG = moveIMG;
exports.moveCSS = moveCSS;
exports.build = build;
exports.dev = dev;

/* Версия с gulp.task */
// gulp.task("moveCss", () =>{
// 	return gulp.src("./src/css/*.css")
// 		.pipe(gulp.dest("./dist/css/"));
// });
//
// const watchers = () => {
// 	gulp.watch('./src/css/*.css', gulp.series('moveCss'));
// }
// gulp.task("dev", gulp.series('moveCss', watchers));

