const gulp = require("gulp");

const moveJS = () => {
	return gulp.src("./src/scripts/*.js")
		.pipe(gulp.dest("./dist/js/"));
}

const moveIMG = () => {
	return gulp.src("./src/images/**/*.+(jpg|png)")
		.pipe(gulp.dest("./dist/images/"));
}

gulp.task("moveJS", moveJS)
gulp.task("moveIMG", moveIMG)

gulp.task("moveAll", gulp.series(moveJS, moveIMG));


