# Продвинутое использование `display:flex`
Функционал этого инструмента призван спасти нам жизнь не только в создании верстки одного разрешения, но и при построении адаптивных страниц. 

Ключевые свойства мы уже рассмотрели на прошлом занятии - с их помощью можно расположить элементы в простые последовательности.

Но что, если нужно немного больше чем просто создать пару колонок?

### детальнее про `flex-direction`
На самом деле имеет больше чем два значения:
 - `row`
 - `row-reverse` - элементы будут располагаться справа на лево
 - `column`
 - `column-reverse` - элементы будут располагаться снизу вверх

Если выстроить элементы в одну колонку пр помощи этого свойства, то ничего особенного не произойдет - мы просто будет имитировать работу потока как он себя ведет по умолчанию.

Но если при этом `flex-parent`у добавить высоту, появится плацдарм для творчества.

Вот такой набор свойств обеспечит нам сутку из 3 колонок и 2 рядков, где рядки будет с адаптивной высотой:
```css
.fancy-grid {
  height: 535px; /*отвечает за высоту флекс родителя*/
  display: flex; 
  flex-direction: column; /*меняем направление главной оси*/
  flex-wrap: wrap; /*разрешаем дочерним элементам "перепрыгивать" на следующую колонку*/
  justify-content: space-between; /*равномерно распределяем колонки по ширине родителя*/
}

.fancy-grid-item {
  width: calc((100% / 3) - 20px); /*задаем ширину колонки так, чтобы их было 3 и оставляем немного места, чтобы justify-content было куда все раздвигать*/
  margin-bottom: 25px; /*отступ между элементами в колонке*/
}
```

### Смена очередности при помощи `order`
Суть проста. У всех элементов по умолчанию одно и то же значение этого свойства и равно ого нулю.

Элементы с одинаковым значением свойства `order` - на странице располагаются в той очередности, в которой они объявлены в HTML.

Блок с большим значением свойства, становится за блоком с меньшим значением. Т.е. прямо по порядку, как мы и вести счет.

Таким образом мы получаем легальный инструмент для смены очережности элементов. Это позже очень поможет и при drag&drop, и при смене расположения карточек на разных устройствах.

### Загадка `flex-grow` и `flex-shrink`
#### `flex-grow`
Отвечает за то в каком пропорциональном отношении флекс-дети будут увеличиваться в размерах. Базово у всех элементов значение этого свойства - `0`.

С его помощью мы, не задавая размеров, можем отрегулировать размеры всех дочерних элементов. К примеру если кнопка `search` должна быть в 5 раз меньше `input`у, который слева от нее, то кнопке будет дан `flex-frow: 1`, а `input`у - `flex-grow: 5`.

####`flex-shrink`
Похож на `flex-grow`, но отвечает за уменьшение элементов, в случае если элементам некуда деться и нужно уменьшать свои размеры.

Это происходит, если свойству `flex-wrap` установлено значение `no-wrap`, иначе элементы так и не начав уменьшаться - перепрыгнут на следующую строку. 

## Удобные short-hand свойства

#### `flex-flow`
Объединяет в себе два свойства - `flex-direction` и `flex-wrap`. Позволяет быстрее и удобнее задавать направления осей в флекс-родителе.

#### `flex`
В одном свойстве можно указать сразу `flex-grow`, `flex-shrink` и `flex-basis`. Более полезным будет при освоении адаптивной верстки.

## **Выравнивание вдоль главной оси** 

```css
.justify-content {
  	justify-content: flex-start;/* Выравнивание флекс-элементов с начала */
    justify-content: flex-end; /* Выравнивание элементов в конце */
	justify-content: center; /* Выравнивание элементов по центру */
 	justify-content: space-between; /* Равномерно распределяет все элементы по ширине flex-блока. Первый элемент вначале, последний в конце */
	justify-content: space-around; /* Равномерно распределяет все элементы по ширине flex-блока. Все элементы имеют полноразмерное пространство с обоих концов */
}
```



## **Выравнивание поперёк главной оси** 

```css
.align-items {
  	align-items: stetch; /* Растянуть предметы, чтобы соответствовать */
	align-items: flex-start;
	align-items: flex-end;
	align-items: center; /* Центрировать элементы в поперечной оси */
	align-items: baseline; /* Выровняйте базовые линии предметов */
}
```
