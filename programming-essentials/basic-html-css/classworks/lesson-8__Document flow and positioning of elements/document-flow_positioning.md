## Поток
В HTML формирование элементов на странице происходит сверху вниз согласно схеме документа. 

Элемент, размещенный в самом верху кода, отобразится раньше слоя, который расположен в коде ниже.  Такая логика позволяет легко прогнозировать результат вывода элементов и управлять им. 

Порядок вывода объектов на странице и называется «потоком». При этом существует несколько возможностей «вырвать» элемент из потока, а значит сделать его невидимым для остальных элементов. Раз он не существует в потоке, то в коде его можно описать где угодно, а также выводить в заданное место окна.

## Float-элементы
Float-элемент находится **вне потока**. Разумеется все остальные элементы на странице ведут себя так, будто float'а не существует и для них это действительно так.

За одним исключением - текст внутри элементов будет обтекать float.

Вырвать элемент из потока при помощи float'ов можно, прикрепив его к одной из сторон - `left` или `right`.

Тогда блок станет невидимым для всех, кроме текста и "прилипнет" в соответствующей стороне документа.

Если float'ов несколько, то они знают о существовании друг друга и будут размещены согласно очередности в разметке.

#### Зачистка потока
Часто становится важным контролировать эту способность текста обтекать float-элементы.
 
 Для того, чтобы подвести некую черту, дальше которой это не будет работать - используется свойство `clear` с одним из значений - `left`, `right` или `both`.
 
 таким образом можно контролировать с каким именно слоем float-элементов мы будем иметь дело.
 
 Имеет место и более радикальный подход, когда в нужный момент на странице создается пустой блок, которому и прописывается свойство `clear: both` - эта конструкция зачистит поток.
 
 Несовершенство в том, что мы создали лишний элемент в разметке, следить за которым весьма сложно, если прийдется добавлять контент на странице до или после него. 
 
 Для решения этой проблемы есть решение с использованием псевдо-элемента `::after`. Это решение принято создавать в классе с именем `clearfix`:
 ```css
.clearfix::after {
  content: '';
  display: block;
  clear: both;
}
``` 
Такой класс может быть присвоен контейнеру, внутри которого мы будем использовать `float`.

## Позиционирование

За позиционирование элемента в потоке отвечает свойство `position`. Значение по умолчанию - `static` - отвечает за базовое поведение к которому мы привыкли. Остальные, так или иначе, элемент из потока вырывают.

* `relative` - место, которое занимает элемент резервируется и никем не может больше быть занято, но сам элемент мы можем перемещать куда угодно.

* `fixed` - фиксирует элемент в одном положении относительно окна браузера.

* `absolute` - вырывает элемент из потока, он будет спозиционирован относительно ближайшего родительского, у которого свойство `position` любое, кроме `static`. 

Конкретную позицию, вырванного из потока элемента, можно задать при помощи свойств `top`, `left`, `right`, и `bottom`.