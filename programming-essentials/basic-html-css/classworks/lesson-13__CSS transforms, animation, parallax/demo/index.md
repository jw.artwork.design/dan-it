midpoint. A handful of the more popular transitional properties include the following.

- `height`
- `width`
- `padding`
- `max-height`
- `max-width`
- `min-height`
- `min-width`
- `font-size`
- `transform;`


- `opacity`
- `margin`
- `border-width`
- `left`
- `bottom`
- `right`
- `top`
- `line-height`
- `background-color`
- `background-position`
- `border-color`
- `border-spacing`
- `font-weight`
- `text-shadow`
- `visibility`
- `z-index`