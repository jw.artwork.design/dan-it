# Git команды

0. **git config --global --list**

    **git config --global user.name NAME**

    **git config --global user.email EMAIL**

    ⚠ Результат 

    $ git config --global --list

    filter.lfs.clean=git-lfs clean -- %f

    filter.lfs.smudge=git-lfs smudge -- %f

    filter.lfs.process=git-lfs filter-process

    filter.lfs.required=true

    user.name=Igor.T

    user.email=igor.t@gmail.com 

    

1. **git clone [Cсылка на репозиторий] [Название папки]**

   ⚠ Результат
   $ git clone https://gitlab.com/dan-it/groups/pe43.git

   Cloning into 'gitstart'...
   warning: You appear to have cloned an empty repository.

   


2)   **git checkout -b [Имя ветки]** – создание новой ветки и переход на нее

	⚠ Результат
	$ git checkout -b start
	
	Switched to a new branch ‘start'



3)   **git add .**  – Добавление всех новых файлов

	⚠ Результат
	$ git add .
	
	
	
4)   **git rm [--cached] [file name]** – удаление файлов

     ⚠ Результат

     $ git rm --cached start.md

     rm 'start.md' 



5. **git status** – проверка модифицированных файлов

   ⚠ Результат
   git status

   On branch master
   No commits yet
   Untracked files:
   (use "git add <file>..." to include in what will be committed)
   start.md
   nothing added to commit but untracked files present (use "git add" to track)



6. **git commit -m "Massage"**  - создание комита

   ⚠ Результат
   $ git commit -m 'finish header component'

   [master (root-commit) 63b2780] finish header component
   1 file changed, 39 insertions(+)
   create mode 100644 start.md



7. **git push** – отправка изменений в репозиторий

   ⚠ Результат
   $ git push

   Enumerating objects: 4, done.
   Counting objects: 100% (4/4), done.
   Delta compression using up to 4 threads
   Compressing objects: 100% (3/3), done.
   Writing objects: 100% (3/3), 274.02 KiB | 14.42 MiB/s, done.
   Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
   To https://gitlab.com/dan-it/groups/pe39.git
      0459662..11f63d9  master -> master



8. **git pull** – стянуть изменения с репозитория

   ⚠ Результат 

   $ git pull



# Создать новый репозиторий 

1. Решить куда клонировать репозиторий

2. Открыть терминал(MacOS)/Git Bash(Windows) и прейти в нее при помощи команды 

   cd 'ссылка на папку, просто претащить папку в терминал'

3. git clone [Cсылка на репозиторий]

4. Ввести логин и пароль от сайта [GitLab/GitHub] то где у вас создар репозиторий

5. cd [Название папки] что бы перейти в папку репозитория

6. Делаем наши изменения в репозитории 

7. Затем делаем git add [имя файла который изменили или добавили]

8. git commit -m "Текст сообщения что было сделано"

9. git push новыйе или измененные файлы отправляем на сервер

   **При каждом изменеии в репозитории повторяем этапы 5,6,7,8**

10. git pull - что бы затянуть изменения из GitLab


