### ПРАКТИЧЕСКИЕ ЗАДАНИЯ
* Вычислить вес селекторов:
* `div > a + .classname`  0012
* `.classname a`  0011
* `section.hello`  0011
* `section a:hover`  0012
* `section .classname #element`  0111
* `.classname[href='#']` 0020
* `div > p .classname-99` 0012
* `.classname a span.classname::before` 0023
* `style="color: #000;"` 1000

