# Свойство display
То, без чего ни один элемент не появился бы на странице.

У него есть три базовых значения, отличия между которыми важнее всего. Понимая эти различия, можно создавать верстку с пониманием дела и избежать большей части не обязательных свойств.

### `display: block`
Все бочные элементы именно так и становятся блочными.

Это значит, что каждый из них будет занимать 100% ширины родительского элемента.

Если блочному элементу задать высоту и/или ширину все так и будет. Но в любом случае за ним будет резервироваться оставшееся место.
Т.е. следующий элемент сможет появиться только под ним.

### `display: inline`
Этот тип элементов предназначен для того, чтобы выделить небольшой участок текста и, **не нарушая общую структуру**, отформатировать его отдельно.

Яркий пример - ссылка. Среди длинного абзаца встречается слово, по клику на которое пользователь должен перейти на страницу википедии со статьей по этому поводу.
Мы его заворачиваем в ссылку и все работает.

Для того, чтобы инлайновые(строчные) элементы не нарушали структуру текста - для них **не работает** высота и вертикальные отступы.

Вот такие стили для ссылки:
```css
.hyper-link {
  padding-top: 60px;
  height: 25px;
}
```
сработают, но будут проигнорированы всеми остальными элементами, потому что иначе это противоречило бы предназначению `display: inline`.

### `display: inline-block`
Взяли лучшее от предыдущих. Работает и ширина и высота и отступы во все стороны.

Более того - если на страницу по ширине влазит два или больше таких - они непременно выстроятся слева на право по одному.

Есть нюанс. Между блочно-строчными элементами будет маленькое расстояние.

Случится это только если написать с разделением. Не важно разделяем мы их пробелами или переносом строки - в браузере этот символ будет занимать место. Размер такого разделителя, зависит на прямую от размера шрифта родителя.

# Flexbox
### `display: flex`
Дарит нам удобство позиционирования элементов. Все, что раньше вызывало неудобства теперь делается в пару строк для одного элемента.

Основное новшество - это то, что почти все свойства мы присваиваем родительскому элементу. Именно там происзодит контроль поведения всех дочерних. Таким образом мы теперь делим элементы на `flex-parent` и `flex-child`-ы.

Если просто присвоить какому-то блоку с дочерними элементами `display: flex`, много хорошего сразу не произойдет, само собой.
Изначально все дочерние элементы будут стараться поделить поровну всю ширину родителя, сколько бы таких элементов внутри не было. Иногда и этого достаточно.

Но чаще всего поведение элементов нужно настраивать дальше.

Рассмотрим базовые свойства для этого:
 - `flex-wrap` - отвечает за перенос элементов, если они не помещаются. 4 элемента с шириной 400 пикселей не влезут ровно в контейнер 600 пикселей. Именно это свойство позволит тем, кто не влазит перепрыгнуть ниже
 - `justify-content` - зона ответственности - как элементы будут делить оставшееся пустое место. С его помощью можно равномерно распределить на равные промежутки элементы друг от друга и тд.
 - `align-items` - это вертикальное выравнивание. Когда соседствуют два flex-child'а с разной высотой, данное свойство расскажет им как выстроиться - по нижнему, верхнему краю или по центру.
 - `flex-direction` - отвечает за направление в котором будут изначально выстраиваться flex-child'ы. `row` - с лева на право, или `column` - сверху вниз. При этом значение `column` изменит не только расположение элементов. `justify-content` - будет отвечать за вертикальное выравнивание, а `align-items` за горизонтальное.