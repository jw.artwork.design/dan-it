/**
 * Завдання 3.
 *
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Ctrl + D на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */

// get input new-task
// get list
// create function what button is pressed
// if enter add to list our input text
// if Ctrl + D delete last element
// if Clear all OR Alt + Shift + Backspace -delete whole LIST
// if delete all list - confirm isSure?
let list = document.querySelector( ".tasks-list" )
let input = document.querySelector( "#new-task" )
let clearButton = document.querySelector( "#clear" )

const addElem = ( elem, list ) => {
	let createLi = document.createElement( "li" )
	createLi.innerText = elem
	list.append( createLi )
}

const deleteList = ( list ) => {
	list.innerHTML = ""
}

// if Ctrl + D delete last element

const deleteLastLi = ( list ) => {
	list.lastChild.remove()
}

clearButton.addEventListener( "click", () => {
	if ( confirm( "Are you sure?" ) ) {
		deleteList( list )
		//    input.value=""
	}
} )
document.addEventListener( "keyup", ( event ) => {
	if ( event.code === "Enter" ) {
		if ( input.value !== "" ) {
			addElem( input.value, list )
			input.value = ""
		}
	}
	if ( event.code === "MetaRight" || event.ctrlKey && event.code === "KeyQ" ) {
		deleteLastLi( list )
	}
	if ( event.shiftKey && event.code === "Backspace" && event.altKey || event.code === "Option" ) {
		if ( confirm( "Are you sure?" ) ) {
			deleteList( list )
			//    input.value=""
		}
	}
} )

