/**
 * Завдання 5.
 *
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */

// найти counter, decrement, increment
// создать счетчик для + и -
// написать функцию

const counter = document.querySelector( '#counter' );
const decrement = document.querySelector( '#decrement-btn' );
const increment = document.querySelector( '#increment-btn' );

let num = 0;

const decrFn = () => {
	if ( num !== 0 ) {
		num--;
		counter.innerText = `Counter: ${ num }`;
	}
}

const incFn = () => {
	num++;
	counter.innerText = `Counter: ${ num }`;
}

decrement.addEventListener( 'click', decrFn );
increment.addEventListener( 'click', incFn );