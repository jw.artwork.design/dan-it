/**
 * Завдання 1.
 *
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізовує логіку підрахунку натискання клавіші Enter
 * та відображає результат в enter-counter блок
 *
 */

let countEnter = 0;
let countSpace = 0;
let countBackspace = 0;
const enter = document.querySelector( '#enter-counter' );
const space = document.querySelector( '#space-counter' );
const backspace = document.querySelector( '#backspace-counter' );
const counterKey = ( event ) => {
	if ( event.code === 'Enter' ) {
		++countEnter;
		enter.innerText = countEnter;
	} else if ( event.code === 'Space' ) {
		++countSpace;
		space.innerText = countSpace;
	} else if ( event.code === 'Backspace' ) {
		++countBackspace;
		backspace.innerText = countBackspace;
	}
	console.log( event.code );
}
document.addEventListener( 'keyup', counterKey );