/**
 * Завдання 2.
 *
 * Початкове значення кнопки повинно дорівнювати 0
 * При натисканні на кнопку збільшувати це значення на 1
 *
 */

let countButton = 0;
const button = document.querySelector( '.counter' )
button.innerText = countButton;
const countclick = () => {
	countButton++;
	button.innerText = countButton;
}
button.addEventListener( "click", countclick )