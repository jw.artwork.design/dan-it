/**
 * Задание 2.
 *
 * Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.
 *
 * Условия:
 * - Решить задачу грамотно.
 */


const button = document.createElement('button')
button.innerText="Войти";
document.body.append(button);

button.addEventListener('mouseover', () => {
	alert('При клике по кнопке вы войдёте в систему.')
}, {once: true} );

//  const clickFn = () => {
//      alert('добро пожаловать!')
//  }

button.addEventListener('click', () => {
	alert('добро пожаловать!')
})