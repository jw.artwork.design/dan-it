/**
 * Задание 1.
 *
 * Написать скрипт, который создаст элемент button с текстом «Войти».
 *
 * При клике по кнопке выводить alert с сообщением: «Добро пожаловать!».
 */

// 1. создать элемент
// 2 добавить текст
// 3 добавить в body
// 4 создать функцию
// 5 добавить слушатель на click

const button = document.createElement( 'button' );

button.innerText = 'войти';
document.body.append( button );

const clickFn = () => {
	alert( 'Добро пожаловать!' );
}

button.addEventListener( 'click', clickFn ); // нельзя вызывать функцию clickFn()