/**
 * Задание 3.
 *
 * При наведеннии на блок 1 делать блок 2 зеленого цвета
 * А при наведении на блок 2 делать блок 1 красным цветом
 *
 */

// найти блок 1 и блок 2
//при наведениии на блок 1 сменить цвет в блока 2
// при наведении на блок 2 сменить цвет  рна блок 1
// если мы не навелись на какой-то блок цвет не должен быть изменен

const block1 = document.getElementById( 'block-1' );
const block2 = document.getElementById( 'block-2' );

// block1.addEventListener('mouseenter', ()=>{
//     block2.style.backgroundColor = 'green';
// })
// block2.addEventListener('mouseenter', ()=>{
//     block1.style.backgroundColor = 'red';
// })
// block1.addEventListener('mouseleave', ()=>{
//     block2.style.backgroundColor = 'white';
// })
// block2.addEventListener('mouseleave', ()=>{
//     block1.style.backgroundColor = 'white';
// })

document.body.addEventListener( 'mouseover', ( event ) => {
	if ( event.target === block1 ) {
		block2.style.backgroundColor = 'green'
	} else if ( event.target === block2 ) {
		block1.style.backgroundColor = 'red'
	} else {
		block2.style.backgroundColor = 'white';
		block1.style.backgroundColor = 'white';
	}
} )