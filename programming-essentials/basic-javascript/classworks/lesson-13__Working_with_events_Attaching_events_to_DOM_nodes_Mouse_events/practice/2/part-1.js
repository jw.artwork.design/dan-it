/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
	const r = Math.floor( Math.random() * 255 );
	const g = Math.floor( Math.random() * 255 );
	const b = Math.floor( Math.random() * 255 );

	return `rgb(${ r }, ${ g }, ${ b })`;
}


// 1- create h1
// 2- преобразовать в массив
// 3- перебрать массив и присвоить кадждому елемету span
// 4- присвоить всем елементам(кроме пробела)
// create buttom расскрасить
// 6- дать кнопке текст "раскрасить"
// 7-разместить в body эти елементы
// 8- присвоить функци спанам
// 9- дать слушатель клик кнопке

let createH1 = document.createElement( "h1" )

for ( const letter of PHRASE ) {
	let createSpan = document.createElement( "span" );
	createSpan.innerText = letter;
	createH1.append( createSpan )
}

document.body.prepend( createH1 )
let createBtn = document.createElement( "button" )
createBtn.innerText = "Раскрасить"
createH1.after( createBtn )

const getColor = () => {
	let letters = document.querySelectorAll( "span" )
	letters.forEach( item => {
		if ( item.innerText !== " " ) {
			item.style.color = getRandomColor();
		}
	} )
}

createBtn.addEventListener( "click", getColor );
document.addEventListener( "mousemove", getColor )



