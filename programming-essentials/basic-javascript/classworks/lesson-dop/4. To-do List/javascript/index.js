/**
 * Нужно создать функции для создания и работы с to do list
 * Нужно сделать функцию toDoListStart что бы добавить сохраненные из массива data и фикцию startTask которая распределит
 * в какой список нужно добавить список.
 * Нужно сделать функцию createNewTaskElement которая будет создать новую таску
   <input type="checkbox" name="completed-task" />
   <label>Текст</label>
   <input name="text" type="text">
   <button type="button" class="edit">Edit</button>
   <button type="button" class="delete">Delete</button>
 * Нужно сделать функцию addTask которая будет добавлять нашу новую таску в нужны список
 * Нужно будет сделать функции editTask / deleteTask для удаления и редактирования таски
 * Нужно будет сделать функции taskCompleted / taskIncomplete для распределения по спискам
 * Нужно будет сделать функции bindTaskEvents которая навешает событие клика на необходимые кнопки
 *
 * */

const data = [{
	text: "Pay Bills",
	status: "incomplete"
},{
	text: "See the Doctor",
	status: "completed"
}];
