/**
 * Задание 2.
 *
 * Сделать форму логина с сохранением данных в localStorage.
 */

//1. Создать функцию формс
//2. Сгенерировать форму с 2 инпутами user и pass и 2 кнопками send clear
//3. Найти кнопки и ипнуты
//4. Навешать евент на конпку Send и по условию если такого ключа нету в localStorage то добавить
//5. Навешать евент на конпку Clear которая удалит выбранные ключи в localStorage


const forms = () => {
	document.querySelector('.root').insertAdjacentHTML('beforeend', `<form action=''>
			<p>
				<label>
					<span>User</span><br>
					<input name='user' type='text'>
				</label>
			</p>
			<p>
				<label>
					<span>Password</span><br>
					<input name='pass' type='password'>
				</label>
			</p>
			<p><button id='send' type='button'>Send</button> <button id='clear' type='button'>Clear</button></p>
		</form>`)

	let user = document.querySelector("[name='user']");
	let password = document.querySelector("[name='pass']");
	let btnSend = document.querySelector("#send");
	let btnClear = document.querySelector("#clear");

	btnSend.addEventListener('click', () =>{
		if ( !localStorage.getItem('user') ){
			localStorage.setItem('user', user.value);
		}
		if ( !localStorage.getItem('pass') ){
			localStorage.setItem('pass', password.value);
		}
	});

	btnClear.addEventListener('click', () =>{
		localStorage.removeItem('user');
		localStorage.removeItem('pass');
	})
}

forms()