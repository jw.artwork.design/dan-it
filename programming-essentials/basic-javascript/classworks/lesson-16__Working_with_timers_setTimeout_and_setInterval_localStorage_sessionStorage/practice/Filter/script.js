/**
 * Задание 3.
 *
 * Реализовать фильтр галереи
 * */

const images = [
	[{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	}],
	[{
		data: 'graphic_design',
		src: './images/img1.png'
	},{
		data: 'web_design',
		src: './images/img2.png'
	},{
		data: 'landing_pages',
		src: './images/img3.png'
	},{
		data: 'graphic_design',
		src: './images/img4.png'
	},{
		data: 'web_design',
		src: './images/img5.png'
	},{
		data: 'landing_pages',
		src: './images/img6.png'
	},{
		data: 'graphic_design',
		src: './images/img7.png'
	},{
		data: 'web_design',
		src: './images/img8.png'
	},{
		data: 'landing_pages',
		src: './images/img9.png'
	},{
		data: 'graphic_design',
		src: './images/img10.png'
	},{
		data: 'web_design',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img12.png'
	}],
	[{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	},{
		data: 'landing_pages',
		src: './images/img11.png'
	}],
]
const SET_TIMEOUT = 5000; //5сек
function Gallery() {

// 1 находим враппер gallery-menu
// 2 Добавляем евентлиснер gallery-menu
// 3 Собираем колекцию data-gallery
// 4 Получаем значение data-memu у нажитой кнокбки
// 5 Отсортировать data-gallery по критерию из 4 пункта
// 6 все что не подходит дать элементам display: none;
// 7 Добавить условие для data-memu: all, нужно показать все картинки.

	let cout = 0;
	const galleryMenuWrapper = document.querySelector('.gallery-menu');
	const loadMore = document.querySelector('.load_more');
	const galleryContent = document.querySelector('.gallery-content');

	galleryMenuWrapper.addEventListener('click', (event) => {
		let dataMemu = null;
		const dataGallery = document.querySelectorAll('.image-item');

		if ( event.target.nodeName === "BUTTON" ) {
			dataMemu = event.target.getAttribute( 'data-menu' );
		}

		let menuItems = document.querySelectorAll( '[data-menu]' );

		menuItems.forEach( item => {
			if ( item.classList.contains( 'active' ) ) {
				item.classList.remove( 'active' );
			}
		} );

		if ( !event.target.classList.contains( 'active' ) ) {
			event.target.classList.add( 'active' );
		}

		dataGallery.forEach((item) => {
			if (dataMemu !== item.getAttribute( 'data-gallery' ) && dataMemu !== 'all' ){
				item.style.display = 'none'
			} else {
				item.style.display = 'block'
			}
		})
	})

	loadMore.addEventListener('click', () => {

		loadMore.classList.add('loader');
		// loadMore.setAttribute('disabled', '')


		setTimeout(() => {
			loadMore.classList.remove('loader');
			// loadMore.removeAttribute('disabled')

			// Если нам нужно что бы подгружалось по 12 тогда мы можем создать себе массив данных нужного формата которысый состоит из 2 массивов по 12 обьектов
			// и что бы за каждым кликом обращатся к нужному куску мы можем делать каунт клика кторый будет являтся индексом в галавном массиве и будем обращяться вот так images[cout]
			images[cout].forEach((image, id) => {
				galleryContent.insertAdjacentHTML('beforeend', `
			<div class="image-item" data-gallery="${image.data}">
				<img class="img${id+13}" src="${image.src}" alt="image">
			</div>`)
			})

			cout++

			if ( cout === images.length ){
				loadMore.remove()
			}
		}, SET_TIMEOUT)

	})

}

document.addEventListener('DOMContentLoaded', () => {
	Gallery();
});
