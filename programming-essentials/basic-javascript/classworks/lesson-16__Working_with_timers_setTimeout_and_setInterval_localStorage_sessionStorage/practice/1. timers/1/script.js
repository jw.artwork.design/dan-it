/**
 * Задание 1.
 *
 * Написать программу для напоминаний.
 *
 * Все модальные окна реализовать через alert.
 *
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */


// 1. Знайти інпути і кнопку
// 2. Функція напоминлка
// 3. Провірки щоб не було 0
// 4. Провірка на пустий текст
// 5. Провірка на валіднімть данних
// 6. Функція сет тайм для напоминалки


let inputMassege = document.querySelector( 'input[type="text"]' );
let inputTime = document.querySelector( 'input[type="number"]' );
let button = document.querySelector( 'button' );

const remaind = () => {
	let inputMassegeValue = inputMassege.value;
	let inputTimeValue = Number( inputTime.value );
	if ( inputMassegeValue.length === 0 ) {
		alert( 'Введіть текст напоминалку' );
	}
	if ( inputTimeValue < 1 ) {
		alert( 'Час затримки повинен бути більше однієї секунди' );
	}
	button.setAttribute( 'disabled', '' );
	const intervalId = setInterval( () => {
		inputTime.value = --inputTimeValue;
	}, 1000 );
	setTimeout( () => {
		alert( `${ inputMassegeValue }` );
		clearInterval( intervalId );
		button.removeAttribute( 'disabled' )
	}, inputTimeValue * 1000 );
}

button.addEventListener( 'click', remaind );

window.addEventListener( 'load', () => {
	inputMassege.focus()
} );