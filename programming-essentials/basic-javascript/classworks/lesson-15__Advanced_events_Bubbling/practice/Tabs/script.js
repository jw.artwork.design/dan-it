/**
 * Задание 4.
 *
 * Реализовать табы
 * */

//1. Находим таб меню враппер
//2. Создать функцию change
//3. в функции сделаем колекцию кнопок
//4. в функции сделаем переберем и удалим если есть активные кнопки
//5. в функции сделаем проверку на добавление активного состояния на кнопке
//6. в функции найдем id активной меню
//7. в функции сделаем колекцию контентов
//8. в функции проверим на наличие активного состояния у контента и удалим если есть и назначим тому кто соотведствует кнопки

const tabsFn = () => {
	let tabs = document.querySelector( '.tab-menu' );

	const change = ( event ) => {
		let menuItems = document.querySelectorAll( '[data-tab]' );

		menuItems.forEach( item => {
			if ( item.classList.contains( 'active' ) ) {
				item.classList.remove( 'active' );
			}
		} );

		if ( !event.target.classList.contains( 'active' ) ) {
			event.target.classList.add( 'active' );
		}

		let id = event.target.getAttribute( 'data-tab' );
		let conteiners = document.querySelectorAll( '.content-item' );

		conteiners.forEach( item => {
			if ( item.classList.contains( 'active' ) ) {
				item.classList.remove( 'active' );
			}
		} );

		if ( !document.getElementById( id ).classList.contains( 'active' ) ) {
			document.getElementById( id ).classList.add( 'active' );
		}
	}

	tabs.addEventListener( 'click', ( event ) => {
		if ( event.target.nodeName === 'BUTTON' ) {
			change( event );
		}
	} );

}
tabsFn();