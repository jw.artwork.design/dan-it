/**
 * Завдання 2.
 *
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 *
 * ADVANCED: Додати можливість видаляти елементи зі списку покупок
 * (додати Х в кінець кожного елементу, при кліку на який відбувається
 * видалення) та відмічати як уже куплені (елемент перекреслюється при
 * кліку на нього)
 * Для реалізації цього потрібно розібратися з Event delegation
 * https://javascript.info/event-delegation
 */

//1. Найти инпут
//2. Найти кнопку
//3. Найти список
//4. Сделать функцию добавление элемента в список
//5. Сделать функцию которая будет перечекивать выполненый пункт
//6. Сделать функцию которая будет удалять элемент списка

let input = document.querySelector( "#new-good-input" )
let button = document.querySelector( "#add-btn" )
let list = document.querySelector( ".shopping-list" )

let addLi = ( input, list ) => {
	list.insertAdjacentHTML( "beforeend", `<li>${ input.value }<button type="button">X</button></li>` );
	input.value = "";
}

let lineThrough = ( event ) => {
	if ( event.target.nodeName === "LI" ) {
		event.target.classList.toggle( "line-through" );
	}

	console.log( event.target );
}

let removeItem = ( event ) => {
	if ( event.target.nodeName === "BUTTON" ) {
		event.target.parentNode.remove();
	}
}

button.addEventListener( "click", () => {
	addLi( input, list );
} )

document.addEventListener( "keyup", ( event ) => {
	if ( event.code === "Enter" ) {
		addLi( input, list );
	}
} )

list.addEventListener( "click", ( event ) => {
	lineThrough( event );
	removeItem( event );
} )
