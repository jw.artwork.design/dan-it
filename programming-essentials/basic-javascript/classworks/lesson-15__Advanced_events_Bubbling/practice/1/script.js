/**
 * Задача 1.
 *
 * Необходимо «оживить» навигационное меню с помощью JavaScript.
 *
 * При клике на элемент меню добавлять к нему CSS-класс .active.
 * Если такой класс уже существует на другом элементе меню, необходимо
 * с того, предыдущего элемента CSS-класс .active снять.
 *
 * У каждый элемент меню — это ссылка, ведущая на google.
 * С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот внешний ресурс.
 *
 * Условия:
 * - В реализации обязательно использовать приём делегирования событий (на весь скрипт слушатель должен быть один).
 */

//1. собрать колекцию ссылок
//2. найти враппе меню
//3. сделать функцию которая будет проверять где таргет сработал и удалять активное состояние и добавляет там где был клик

const links = document.querySelectorAll('a');
const listWrap = document.querySelector('ul');

listWrap.addEventListener('click', (event) => {
	event.preventDefault();
	console.log(event.target);
	console.log(event);

	if (event.target.nodeName === "A") {
		links.forEach((tab) => {
			if (tab.classList.contains('active')){
				tab.classList.remove('active');
			}
		});

		if (!event.target.classList.contains('active')){
			event.target.classList.add('active');
		}
	}


});