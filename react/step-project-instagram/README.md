## Завдання

Реалізувати веб-додаток, аналог [Instagram](https://www.instagram.com/).

#### Командна робота

На цьому проекті всі студенти поділені на групи по три особи. Учасники команди можуть самостійно вирішити, як розподілити завдання щодо проекту. Термін виконання проекту – 2 тижні.

#### Технічні вимоги:

Додаток повинен містити такі сторінки та функціонал:

**1. Головна сторінка**
   
   Складові сторінки:
   - Стрічка з постів (ліва частина скріншота нижче). Кожен пост містить у собі - фотографію, кнопку лайку, поле для додавання коментаря, та коментарі, якщо вони до посту є. Коментар має відображатися лише останній. Якщо коментарів більше ніж 1 - повинна з'являтися кнопка `Показати більше`, по кліку на яку - відображаються всі коментарі. По подвійному кліку на фотографію – пост лайкається. Коли піст лайкається – іконка лайка має забарвитися в інший колір. Перші три пости завантажуються відразу, інші пости повинні завантажуватися за допомогою технології Infinity scroll. Для реалізації можна використати будь-яку бібліотеку, або реалізувати самостійно.
   - Список людей, на яких людина підписана (права верхня частина скріншота). Кожна "людина" - це іконка та нікнейм. Це має бути клікабельне посилання, при натисканні на яке ви повинні потрапити на сторінку постів цієї людини.
   - Ссисок людей, на яких можна підписатися (права нижня частина скріншота). Кожна "людина" - це іконка, нікнейм, і кнопка `Підписатись`. Іконка і нікнейм - клікабельне посилання, при натисканні на яке ви повинні потрапити на сторінку постів цієї людини. Натискання кнопки `Підписатися` - система підписує вас на оновлення цієї людини. Назва кнопки змінюється на `Відписатись`. До цього списку не повинні потрапити люди, на яких людина вже підписана. При перезавантаженні сторінки люди, на яких ви підписалися в другому списку, повинні бути показані вже в першому списку. 

   Приклад - ![степ проект](./img/1.png)

**2. Сторінка постів конкретної людини**
   
  Складові сторінки:
   - Нікнейм людини + кнопка `Підписатись`. Якщо користувач вже підписаний на цю сторінку, замість кнопки `Підписатись` має бути кнопка `Відписатись`.
   - Пости людини у хронологічному порядку у вигляді сітки. Кожен пост є фотографією. По наведенню на фотографію - вона затемняється, зверху показується кількість лайків та коментарів із відповідними іконками.
   - По кліку на одну з фотографій - з'являється модальне вікно, яке містить у собі фотографію та коментарі до посту. При натисканні на затемнену область зовні модального вікна, воно має закриватися.

   Приклад:
   
   ![степ проект](./img/2.png)
   ![степ проект](./img/3.png)

Дизайн додатку може бути будь-яким. За основу можна взяти стилі справжнього Instagram. Також можна вигадати свій дизайн і реалізувати його. Головне, щоб були виконані всі умови наповнення сторінки контентом, прописані в вимогах вище.

Для стилізації можна використовувати будь-яку бібліотеку (Bootstrap, Bulma, MaterialUI і т.д) або писати всі стилі самостійно.

У проекті має використовуватися SCSS (крім випадку використання MaterialUI та/або JSS).

Також додаток потрібно обов'язково розмістити в інтернеті. Деплой може бути як на Heroku, так і на Amazon, на розсуд команди.

Дані, що виводяться на сторінку, мають бути статичними. Для цього, команді потрібно створити кілька колекцій MongoDb, за допомогою Node.js отримати потрібні дані, і вивести їх на сторінки. Структура колекцій – на розсуд команди.

Обов'язково необхідно покрити фронт-енд частину програми юніт-тестами.

У роботі можна використовувати будь-які npm-модулі на розсуд команди.

Також необхідно створити файл [README.MD](https://dan-it.gitlab.io/fe-book/teamwork/readme.html) в корені проекту з описом проекту, списком учасників та інструкцією із запуску проекту.

#### Необов'язкове завдання підвищеної складності

- Додати можливість творити власні пости. При додаванні поста – обов'язковим є завантаження фотографії.
- Реалізувати власну авторизацію та можливість роботи програми з декількома користувачами.
- Написати юніт-тести для бек-енду